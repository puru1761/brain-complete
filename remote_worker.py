from playground.network.common.Protocol import MessageStorage
from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.StandardMessageSpecifiers import UINT1, UINT4, BOOL1, LIST, STRING
from playground.utils.ui import CLIShell, stdio
from playground.twisted.endpoints import GateServerEndpoint, GateClientEndpoint, PlaygroundNetworkSettings
import time, traceback, logging
from twisted.internet.protocol import Factory, Protocol
from twisted.internet import reactor
from twisted.internet.defer import Deferred
import ParserFunctions as parser
import time

class CommandAndControlRequest(MessageDefinition):
    PLAYGROUND_IDENTIFIER = "my.commandandcontrol.message"
    MESSAGE_VERSION = "1.0"
    
    COMMAND_NOOP = 0
    COMMAND_MOVE = 1
    COMMAND_LOOK = 2
    COMMAND_WORK = 4
    COMMAND_UNLOAD = 5
    COMMAND_INVENTORY = 6
    COMMAND_LOCATION = 7
    COMMAND_EXPLORE = 8
    
    BODY = [("reqType", UINT1),
            ("ID", UINT4),
            ("parameters",LIST(STRING))
            ]
    
class CommandAndControlResponse(MessageDefinition):
    PLAYGROUND_IDENTIFIER = "my.commandandcontrol.response"
    MESSAGE_VERSION = "1.0"
    BODY = [("reqID", UINT4),
            ("success", BOOL1),
            ("message", STRING)]

class RemoteWorkerBrain(object):
    def __init__(self):
        self.__running = True
        self.__lastError = None
        self.__locX = 0
        self.__locY = 0
        self.__map = [[0 for x in range(100)] for y in range(100)]
        self.markMap("unknown", (0,0), (100, 100))
        self.lastKnownFarm = ()
        self.lastKnownCity = (91,91)

    def markMap(self, markType, fromXY, toXY):
        fromX, fromY = fromXY
        toX, toY = toXY
        for x in range(fromX, toX):
            for y in range(fromY, toY):
                self.__map[x][y] = markType

    def stop(self):
        self.__running = False
    
    def findNearest(self, TerrainType, currentLocation):
        if TerrainType == "farmland":
            if self.lastKnownFarm:
                return self.lastKnownFarm
            else:
                for i in range(100):
                    for j in range(100):
                        if self.__map == TerrainType:
                            return (i,j)
                        return False
        elif TerrainType == "city":
            return self.lastKnownCity

    def MoveToLocation(self, ctx, currentLocation, location):
        xDistance = 0
        yDistance = 0
        if location[0] > currentLocation[0]:
            xDistance = location[0] - currentLocation[0]
            direction = "east"
            for i in range(xDistance):
                time.sleep(1)
                res, resM = ctx.api.move(direction)
                print res, resM
            if location[1] > currentLocation[1]:
                yDistance = location[1] - currentLocation[1]
                direction = "south"
                for i in range(yDistance):
                    time.sleep(1)
                    res, resM = ctx.api.move(direction)
                    print res, resM
            else:
                yDistance = currentLocation[1] - location[1]
                direction = "north"
                for i in range(yDistance):
                    time.sleep(1)
                    res, resM = ctx.api.move(direction)
                    print res, resM
        else:
            xDistance = currentLocation[0] - location[0]
            direction = "west"
            for i in range(xDistance):
                time.sleep(1)
                res, resM = ctx.api.move(direction)
                print res, resM
            if location[1] > currentLocation[1]:
                yDistance = location[1] - currentLocation[1]
                direction = "south"
                for i in range(yDistance):
                    time.sleep(1)
                    res, resM = ctx.api.move(direction)
                    print res, resM
            else:
                yDistance = currentLocation[1] - location[1]
                direction = "north"
                for i in range(yDistance):
                    time.sleep(1)
                    res, resM = ctx.api.move(direction)
                    print res, resM

        res, resM = ctx.api.location()
        resultMessage =  "Moved to: "+resM
        return res, resultMessage
    
    def processRequest(self, ctx, msg):
        if msg.reqType == CommandAndControlRequest.COMMAND_NOOP:
            result = True
            resultMessage = "Heartbeat"
            
        elif msg.reqType == CommandAndControlRequest.COMMAND_MOVE:
            direction = msg.parameters[0]
            if len(msg.parameters) > 1:
                count = int(msg.parameters[1])
                if count < 1:
                    raise Exception("Cannot move less than 1 times")
            else: count = 1
            
            # use blocking to make sure each move completes before the next one
            if count > 1: ctx.api.setBlocking(True)
            for i in range(count-1):
                result, resultMessage = ctx.api.move(direction)  # convert to int if necessary
                yield result, resultMessage
            # this result, resultMessage pair is yielded at the end of the function
            result, resultMessage = ctx.api.move(direction)
            ctx.api.setBlocking(False)
            
        elif msg.reqType == CommandAndControlRequest.COMMAND_LOOK:
            # do look, get data, send back
            result, resultMessage = ctx.api.look()
            
        elif msg.reqType == CommandAndControlRequest.COMMAND_WORK:
            result, resultMessage = ctx.api.work()
        
        elif msg.reqType == CommandAndControlRequest.COMMAND_UNLOAD:
            result, resultMessage = ctx.api.unload()
            
        elif msg.reqType == CommandAndControlRequest.COMMAND_INVENTORY:
            result, resultMessage = ctx.api.inventory()
            
        elif msg.reqType == CommandAndControlRequest.COMMAND_LOCATION:
            result, resultMessage = ctx.api.location()

        elif msg.reqType == CommandAndControlRequest.COMMAND_EXPLORE:
                currentType = "land"
        	while True:
        	    xMin = 5
		    yMin = 5
		    xMax = 95
		    yMax = 95
		    res, ResultMessage = ctx.api.inventory()
                    yield res, ResultMessage
		    inventory_size = parser.parseInventory(ResultMessage)
		    if inventory_size<5:
		        target = "farmland"
		    else:
		        target = "city"
		    
		    result, resultMessage = ctx.api.location()
                    yield result, resultMessage
		    self.__locX, self.__locY == parser.parseLocation(resultMessage)
		    currentLocation = parser.parseLocation(resultMessage) 
		    # look around
		    result, resultMessage = ctx.api.look()
                    yield result, resultMessage
		    lookRes = parser.parseLook(resultMessage)

		    #currentType = self.__map[self.__locX][self.__locY]
                    yield True, "The Current Terrain type is: "+currentType
		    #for a currently empty inventory, work till inventory is full
		    if currentType == "farmland" and target == "farmland":
		        res, resM = ctx.api.location()
                        print res, resM
		        farmLocation = parser.parseLocation(resM)
		        self.lastKnownFarm = farmLocation
		        while inventory_size<=5:
                            time.sleep(3)
		            res, resM = ctx.api.work()
                            yield res, resM
		            # Use this to see whether food items are still available or not
		            #iswork = parser.parseWork(resM)
		            if res:
		                inventory_size+=1
		            else:
		                time.sleep(2)
		        target = "city"
                        r, rM = ctx.api.inventory()
                        yield r, rM
                        res, resM = ctx.api.location()
                        yield res, resM
                        currentLocation = parser.parseLocation(resM)
                        res, resM = self.MoveToLocation(ctx, currentLocation, self.lastKnownCity)
                        yield res, resM+"\n\nMoved to City\n"
                        currentType = "city"

		    elif currentType == "land":
                        yield True, "Location Confirmed, Moving now to "+target
		        # think of where to go next
		        if target == "farmland":
		            farmLocation = self.findNearest("farmland",currentLocation)
		            if farmLocation:
		                res, resM = self.MoveToLocation(ctx, currentLocation, farmLocation)
                                yield res, resM+"\n\nFarm Found, Moved to Farm\n"
                                currentType = "farmland"
		            else:
                                print "Farm does not exist in memory now searching . . ."
                                
                                ## This algorithm only traces the edges of the map
                                # Further Functionality needs to be implemented in order to make it trace entire map in spiral
                                # Once the Farm is found, manhattan distance is used to traverse between farm and City.


				xMinFlag, yMinFlag, xMaxFlag, yMaxFlag, farmFound = False, False, False, False, False
                                while not farmFound:
				        if currentLocation[0] > xMin and not xMinFlag:
				            res, resM = self.MoveToLocation(ctx, currentLocation, (currentLocation[0]-10, currentLocation[1]))
                                            yield res, resM
				            res, resM = ctx.api.look()
				            nearby_terrain = parser.parseLook(resM)
				            for terrain in nearby_terrain:
				                if 'farmland' in terrain:
				                    farmFound = True
				                    self.lastKnownFarm = terrain[2]
				                    res, resM = ctx.api.location()
				                    currentLocation = parser.parseLocation(resM)
				                    res, resM = self.MoveToLocation(ctx, currentLocation, self.lastKnownFarm)
                                                    yield res, resM+"\n\nFarm Found\n"
				                    currentType = "farmland"
				                    break
				                else:
				                    res, resM = ctx.api.location()
				                    currentLocation = parser.parseLocation(resM)
				                    if currentLocation[0] <= xMin:
				                        xMinFlag = True
				        
				        elif currentLocation[1] > yMin and not yMinFlag:
				            res, resM = self.MoveToLocation(ctx, currentLocation, (currentLocation[0], currentLocation[1]-10))
                                            yield res, resM
				            res, resM = ctx.api.look()
				            nearby_terrain = parser.parseLook(resM)
				            for terrain in nearby_terrain:
				                if 'farmland' in terrain:
				                    farmFound = True
				                    self.lastKnownFarm = terrain[2]
				                    res, resM = ctx.api.location()
				                    currentLocation = parser.parseLocation(resM)
				                    res, resM = self.MoveToLocation(ctx, currentLocation, self.lastKnownFarm)
                                                    yield res, resM+"\n\nFarm Found\n"
				                    currentType = "farmland"
                                                    print "Farm Found"
				                    break
				                else:
				                    res, resM = ctx.api.location()
				                    currentLocation = parser.parseLocation(resM)
				                    if currentLocation[1] <= yMin:
				                        yMinFlag = True
				        
				        elif currentLocation[0] < xMax and not xMaxFlag:
				            res, resM = self.MoveToLocation(ctx, currentLocation, (currentLocation[0]+10, currentLocation[1]))
                                            yield res, resM
				            res, resM = ctx.api.look()
				            nearby_terrain = parser.parseLook(resM)
				            for terrain in nearby_terrain:
				                if 'farmland' in terrain:
				                    farmFound = True
				                    self.lastKnownFarm = terrain[2]
				                    res, resM = ctx.api.location()
				                    currentLocation = parser.parseLocation(resM)
				                    res, resM = self.MoveToLocation(ctx, currentLocation, self.lastKnownFarm)
				                    currentType = "farmland"
                                                    yield res, resM+"\n\nFarm Found\n"
				                    break
				                else:
				                    res, resM = ctx.api.location()
				                    currentLocation = parser.parseLocation(resM)
				                    if currentLocation[0] >= xMax:
				                        xMaxFlag = True
				        
				        elif currentLocation[1] < yMax and not yMaxFlag:
				            res, resM = self.MoveToLocation(ctx, currentLocation, (currentLocation[0], currentLocation[1]+10))
                                            yield res, resM
				            res, resM = ctx.api.look()
				            nearby_terrain = parser.parseLook(resM)
				            for terrain in nearby_terrain:
				                if 'farmland' in terrain:
				                    farmFound = True
				                    self.lastKnownFarm = terrain[2]
				                    res, resM = ctx.api.location()
				                    currentLocation = parser.parseLocation(resM)
				                    res, resM = self.MoveToLocation(ctx, currentLocation, self.lastKnownFarm)
				                    currentType = "farmland"
                                                    yield res, resM+"\n\nFarm Found\n"
				                    break
				                else:
				                    res, resM = ctx.api.location()
				                    currentLocation = parser.parseLocation(resM)
				                    if currentLocation[1] >= yMax:
				                        yMaxFlag = True
				        else:
				            res, resM = ctx.api.location()
                                            yield res, resM
				            currentLocation = parser.parseLocation(resM)
				            self.MoveToLocation(ctx, currentLocation, (50, 50))
				            yield False, "\n\nFarm Not Found, Move to Center\n"

		        elif target == "city":
		            cityLocation = self.findNearest("city", currentLocation)
		            res, resM = self.MoveToLocation(ctx, currentLocation, cityLocation)
                            yield res, resM+"\n\nMoved to City\n"
		            currentType = "city"

		    elif currentType == "city":
		        # unload and start heading to farmland
		        # based on inventory size, put this into a loop
		        res, resM = ctx.api.unload()
                        yield res, resM
		        target = "farmland"
		        res, resMessage = ctx.api.location()
                        yield res, resMessage
		        currentLocation = parser.parseLocation(resMessage)
		        location = self.findNearest("farmland", currentLocation)
                        res, resM = ctx.api.look()
                        yield res, resM
		        if location:
		            res, resM = self.MoveToLocation(ctx, currentLocation, location)
		            currentType = "farmland"
                            yield res, resM+"\n\nFound Farm, Moved to Farm\n"
		        else:
		            self.MoveToLocation(ctx, currentLocation, (50,50))
		            currentType = "land"
		            yield False, "\n\nNo Farm Found, Moved to Center of Board\n"
		    else:
		        # for odd cases, based on inventory size, either look for farmland or for city
		        pass
        else:
            result = False
            resultMessage = "Unknown request %d" % msg.reqType
        yield result, resultMessage


    def gameloop(self, ctx):
        logger = logging.getLogger(__name__+".RemoteWorkerBrain")
        logger.info("Starting Game Loop")

        self.__running = True
        cAndC = ctx.socket()
        logger.info("Connect to %s:10001" % (ctx.socket.ORIGIN))
        cAndC.connect("ORIGIN_SERVER", 10001)
        tickCount = 0
        connected = False
        messageBuffer = MessageStorage(CommandAndControlRequest)
        while self.__running:
            tickCount += 1
            if cAndC.connected():
                if not connected:
                    connected = True
                    logger.info("Sending heartbeat at tickcount %d" % tickCount)
                    response = CommandAndControlResponse(reqID=0, success=True, message="Heartbeat %d" % tickCount)
                    cAndC.send(response.__serialize__())
                    
                data = cAndC.recv(timeout=1)
                if not data: 
                    continue
                
                messageBuffer.update(data)
                for msg in messageBuffer.iterateMessages():
                    try:
                        for result, resultMessage in self.processRequest(ctx, msg):
                            response = CommandAndControlResponse(reqID = msg.ID, success=result, message=resultMessage)
                            cAndC.send(response.__serialize__())
                    except Exception, e:
                        response = CommandAndControlResponse(reqID = msg.ID, success=False, message="Error: %s" % e)
                        cAndC.send(response.__serialize__())
            elif tickCount > 10:
                raise Exception("Could not connect to C&C within 10 ticks")
            if not cAndC.connected(): time.sleep(1)

class SimpleCommandAndControlProtocol(Protocol):
    def __init__(self):
        self.storage = MessageStorage(CommandAndControlResponse)
        
    def dataReceived(self, data):
        self.storage.update(data)
        for m in self.storage.iterateMessages():
            self.factory.handleResponse(m)
            
class SimpleCommandAndControl(CLIShell, Factory):
    def __init__(self):
        CLIShell.__init__(self, prompt="[NOT CONNECTED] >> ")
        self.__protocol = None
        self.__reqId = 1
        
    def __nextId(self):
        nextId = self.__reqId
        self.__reqId += 1
        return nextId
        
    def connectionMade(self):
        moveCommandHandler = CLIShell.CommandHandler("move","Tell the bot to move (1=North, 2=South, 3=East, 4=West)",
                                                     mode=CLIShell.CommandHandler.SINGLETON_MODE,
                                                     defaultCb=self.__sendBotMove)
        lookCommandHandler = CLIShell.CommandHandler("look", "Tell the Bot to scan", self.__sendBotLook)
        workCommandHandler = CLIShell.CommandHandler("work", "Tell the Bot to work", self.__sendBotWork)
        inventoryCommandHandler = CLIShell.CommandHandler("inventory", "Get the bot's inventory", self.__sendBotInventory)
        unloadCommandHandler = CLIShell.CommandHandler("unload", "Tell the Bot to unload inventory", self.__sendBotUnload)
        locationCommandHandler = CLIShell.CommandHandler("location", "Get the bot's location", self.__sendBotLocation)
        exploreCommandHandler = CLIShell.CommandHandler("explore", "Execute the Search algorithm", self.__sendBotExplore)
        
        self.registerCommand(moveCommandHandler)
        self.registerCommand(lookCommandHandler)
        self.registerCommand(workCommandHandler)
        self.registerCommand(unloadCommandHandler)
        self.registerCommand(locationCommandHandler)
        self.registerCommand(inventoryCommandHandler)
        self.registerCommand(exploreCommandHandler)
        
        networkSettings = PlaygroundNetworkSettings()
        networkSettings.configureNetworkStackFromPath("./ProtocolStack")
        playgroundEndpoint = GateServerEndpoint(reactor, 10001, networkSettings)
        playgroundEndpoint.listen(self)
        
        self.deferToResponse = Deferred()
        
    def handleResponse(self, resp):
        status = resp.success and "succeeded" or "failed"
        self.transport.write("Got Response from Bot. Operation %s. Message: %s\n" % (status, resp.message))
        self.refreshInterface()
        
    def __sendBotMove(self, writer, *args):
        if not self.__protocol:
            writer("No bot connected\n")
            return
        direction = args[0]
        if len(args) > 1:
            count = args[1]
        else:
            count = "1"
        request = CommandAndControlRequest(reqType=CommandAndControlRequest.COMMAND_MOVE,
                                           ID=self.__nextId(),
                                           parameters=[str(direction), count])
        self.__protocol.transport.write(request.__serialize__())
        
    def __sendBotLook(self, writer):
        if not self.__protocol:
            writer("No bot connected\n")
            return
        request = CommandAndControlRequest(reqType=CommandAndControlRequest.COMMAND_LOOK,
                                           ID=self.__nextId(), parameters=[])
        self.__protocol.transport.write(request.__serialize__())
        
    def __sendBotWork(self, writer, *args):
        if not self.__protocol:
            writer("No bot connected\n")
            return
        request = CommandAndControlRequest(reqType=CommandAndControlRequest.COMMAND_WORK,
                                           ID=self.__nextId(), parameters=[])
        self.__protocol.transport.write(request.__serialize__())
        
    def __sendBotInventory(self, writer, *args):
        if not self.__protocol:
            writer("No bot connected\n")
            return
        request = CommandAndControlRequest(reqType=CommandAndControlRequest.COMMAND_INVENTORY,
                                           ID=self.__nextId(), parameters=[])
        self.__protocol.transport.write(request.__serialize__())
        
    def __sendBotUnload(self, writer, *args):
        if not self.__protocol:
            writer("No bot connected\n")
            return
        request = CommandAndControlRequest(reqType=CommandAndControlRequest.COMMAND_UNLOAD,
                                           ID=self.__nextId(), parameters=[])
        self.__protocol.transport.write(request.__serialize__())
        
    def __sendBotLocation(self, writer, *args):
        if not self.__protocol:
            writer("No bot connected\n")
            return
        request = CommandAndControlRequest(reqType=CommandAndControlRequest.COMMAND_LOCATION,
                                           ID=self.__nextId(), parameters=[])
        self.__protocol.transport.write(request.__serialize__())

    def __sendBotExplore(self, writer, *args):
        if not self.__protocol:
            writer("No bot connected\n")
            return
        request = CommandAndControlRequest(reqType=CommandAndControlRequest.COMMAND_EXPLORE, ID=self.__nextId(), parameters=[])
        self.__protocol.transport.write(request.__serialize__())
        
        
    def buildProtocol(self, addr):
        print "buildProtocol. Somebody is connecting to us"
        if self.__protocol:
            raise Exception("Currently, this C&C only accepts a single incoming connection")
        self.__protocol = SimpleCommandAndControlProtocol()
        self.__protocol.factory = self
        self.transport.write("Got connection from bot\n")
        self.prompt = "[CONNECTED] >> "
        return self.__protocol
    
singleton = RemoteWorkerBrain()
gameloop = singleton.gameloop
stop = singleton.stop

if __name__=="__main__":
    stdio.StandardIO(SimpleCommandAndControl())    
    reactor.run()
            
