def parseLook(responseMessage):
    terrain_list = []

    terrain = responseMessage.split('\n')

    for items in terrain:
        terrain_objects = items.split(" ")
        if len(terrain_objects) == 5 and terrain_objects[0]!="Object:":
            from_cord = terrain_objects[2].lstrip("[")
            to_cord = terrain_objects[4].rstrip("]")
            from_coords = from_cord.lstrip('(').rstrip(')').split(',')
            to_coords = to_cord.lstrip('(').rstrip(')').split(',')
            from_crd = ()
            to_crd = ()

            for from_coord in from_coords:
                from_crd = from_crd + (int(from_coord),)

            for to_coord in to_coords:
                to_crd = to_crd + (int(to_coord),)

            terrain_list.append((terrain_objects[1], from_crd, to_crd))
        else:
            pass

    return terrain_list

def parseLocation(responseMessage):
    x, y = responseMessage.split(",")
    return (int(x), int(y))

def parseInventory(responseMessage):
    inventory = responseMessage.split('\n')
    return len(inventory)
